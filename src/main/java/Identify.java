import java.io.*;

public class Identify {
    /**
     * a,b,c стороны треугольника
     */
   public static int a;
    public static int b;
    public static int c;
    /**
     * alphaC, alphaA, alphaB углы треугольника
     */
    public static double alphaC;
    private static double alphaA;
    private static double alphaB;

    /**
     * заполнение вводом с клавиатуры
     */
    public static double SetAlphaC() throws IOException {
        alphaC = input();
        return alphaC;
    }
    public static int SetA() throws IOException {
        a = input();
        return a;
    }
    public static int SetB() throws IOException {
        b = input();
        return b;
    }

    /**
     * собственно ввод с клавиатуры
     */
    public static Integer input() throws IOException {
        System.out.println("Введите число(сначала сороны, потом угол)");
        InputStream inputStream = System.in;
        Reader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        String str = bufferedReader.readLine();
        return Integer.parseInt(str);
    }

    /**
     * решает треугольник
     */
    public static int getC() {
        c = (int) Math.sqrt(a * a + b * b - 2 * a * b * Math.cos(Math.toRadians(alphaC))) + 1;
        return c;
    }
    public static double getAlphaA() {
        alphaA = Math.toDegrees(Math.asin(a * Math.sin(Math.toRadians(alphaC)) / getC()));
        return alphaA;
    }
    public static double getAlphaB() {
        alphaB = 180 - getAlphaA() - alphaC;
        return alphaB;
    }

    /**
     * идентифицирует треугольник
     */
    public static String IdentifyUgol(int a,int b, double alphaC,int c,double alphaA,double alphaB) {
        String str;
        double maxAngle = Math.max(Math.max(alphaA, alphaB), alphaC);
        double maxSide = Math.max(Math.max(a, b), c);
        double minSide = Math.min(Math.min(a, b), c);
        if (maxAngle >= 90) {
            if (maxAngle == 90) {
                str="прямоугольный"+ " ";
            } else {
                str="тупоугольный"+ " ";
            }
        }
        else {
            str="остроугольный"+" ";
        }
        if (a == b || a == c || b == c) {
            if (maxSide == minSide) {
                str+="равносторонний";
            } else {
                str+="равнобедренный";
            }
        }
        return str;
    }
}

