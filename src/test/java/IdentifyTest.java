
import org.junit.Assert;
import org.junit.jupiter.api.Test;


class IdentifyTest {
    /**
     * Метод комбинаторного покрытия условий
     * нужно 8 тестов
     */
    /**
     * 1-2-3-4-7-8-9-11
     */
    @Test
    void Test1() {
        Identify.a = 3;
        Identify.b = 3;
        Identify.alphaC = 91;
        String str = Identify.IdentifyUgol(3, 3, 91, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("тупоугольный равнобедренный"));
    }

    /**
     * 1-2-3-4-7-11
     */
    @Test
    void Test2() {
        Identify.a = 3;
        Identify.b = 5;
        Identify.alphaC = 91;
        String str = Identify.IdentifyUgol(3, 5, 91, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("тупоугольный "));
    }

    /**
     * 1-2-3-5-7-8-9-11
     */
    @Test
    void Test3() {
        Identify.a = 3;
        Identify.b = 3;
        Identify.alphaC = 90;
        String str = Identify.IdentifyUgol(3, 3, 90, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("прямоугольный равнобедренный"));
    }

    /**
     * 1-2-3-5-7-11
     */
    @Test
    void Test4() {
        Identify.a = 3;
        Identify.b = 4;
        Identify.alphaC = 90;
        String str = Identify.IdentifyUgol(3, 4, 90, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("прямоугольный "));
    }

    /**
     * 1-2-6-7-8-10-11
     */
    @Test
    void Test5() {
        Identify.a = 3;
        Identify.b = 3;
        Identify.alphaC = 60;
        String str = Identify.IdentifyUgol(3, 3, 60, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("остроугольный равносторонний"));
    }

    /**
     * 1-2-6-7-11
     */
    @Test
    void Test6() {
        Identify.a = 3;
        Identify.b = 4;
        Identify.alphaC = 70;
        String str = Identify.IdentifyUgol(3, 4, 70, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("остроугольный "));
    }

    /**
     * 1-2-6-7-8-9-11
     */
    @Test
    void Test7() {
        Identify.a = 7;
        Identify.b = 7;
        Identify.alphaC = 65;
        String str = Identify.IdentifyUgol(7, 7, 65, Identify.getC(), Identify.getAlphaA(),
                Identify.getAlphaB());
        Assert.assertTrue(str.equals("остроугольный равнобедренный"));
    }


}